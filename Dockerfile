
# nginx-centos7
# Here you can use whatever base image is relevant for your application.
FROM r-base

# Here you can specify the maintainer for the image that you're building
LABEL maintainer="Erik van den Bergh <erik.vandenbergh@wur.nl>"

# Export an environment variable that provides information about the application version.
# Replace this with the version for your application.
ENV RSHINY_VERSION=1.2.0
ENV RSHINY_SERVER_VERSION=1.5.9.923

# Set the labels that are used for OpenShift to describe the builder image.
LABEL io.k8s.description="R shiny app" \
    io.k8s.display-name="R shiny" \
    io.openshift.expose-services="8080:http" \
    io.openshift.tags="builder,webserver,html,r,rshiny" \
    # this label tells s2i where to find its mandatory scripts
    # (run, assemble, save-artifacts)
    io.openshift.s2i.scripts-url="image:///usr/libexec/s2i"

# Install the r shiny package 
RUN R -e 'install.packages("shiny", repos="https://cran.r-project.org")'
RUN    apt update \
    && apt install -y --no-install-recommends gdebi \
    && wget -nv https://download3.rstudio.org/ubuntu-14.04/x86_64/shiny-server-${RSHINY_SERVER_VERSION}-amd64.deb \
    && gdebi -n shiny-server-${RSHINY_SERVER_VERSION}-amd64.deb \
    && rm shiny-server-${RSHINY_SERVER_VERSION}-amd64.deb \ 
    && rm -rf /var/lib/apt/lists/* 

# Copy the S2I scripts to /usr/libexec/s2i since we set the label that way
COPY ./s2i/bin/ /usr/libexec/s2i 
RUN chmod ugo+x /usr/libexec/s2i/* \
    && mkdir -p /var/log/shiny-server

# Clean up examples
RUN rm -rf /src/shiny-server/*
RUN    chown shiny:shiny /etc/shiny-server \
    && chown shiny:shiny /srv/shiny-server \
    && chown shiny:shiny /var/log/shiny-server \
    && chown shiny:shiny /var/lib/shiny-server

USER shiny
COPY shiny-server.conf /etc/shiny-server/

# Set the default port for applications built using this image
EXPOSE 8080

# Modify the usage script in your application dir to inform the user how to run
# this image.
CMD ["/usr/libexec/s2i/usage"]
